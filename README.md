# Webarchitects Co-op Service Status

This is a repo for the static status site at https://www.webarch.info/ which is also available at https://webarch.gitlab.io/webarch-status/ 

The script to update the site and the code for the site can be found at https://gitlab.com/webarch/webarch-status

The status updates are found in `status.html`, the latest at the top, GitLab CI is used to generate the `index.html` file, on each git commit, from the `top.html`, `status.html` and `bot.html` files.

For more information about the Webarchitects Co-operative see https://www.webarchitects.coop/

## Install

To install the script to update the status first add your ssh public key to your GitLab account, see below for notes on generating a ssh public key, then check out the code and then create a symlink, when you first connect check the ssh fingerprint against the ones here:

* https://about.gitlab.com/gitlab-com/settings/#ssh-host-keys-fingerprints

```bash
cd 
git clone git@gitlab.com:webarch/webarch-status.git
mkdir ~/bin
cd bin
ln -s ../webarch-status/bin/webarch-status
echo "export PATH=$PATH:$HOME/bin" >> ~/.bashrc
source ~/.bashrc
```

## Run

To post a status update:

```bash
webarch-status "Hello world!"
```

To check if the status update was deployed OK check the pipelines page, it can take two or three minutes for the update to be checked and deployed:

* https://gitlab.com/webarch/webarch-status/pipelines

Note that nothing clever is done regarding escaping quotes or HTML, use single quotes if you want to use HTML attributes and `<br>`'s for line breaks (each status update is a paragraph), for example:

```bash
webarch-status "Our email servers are all up and running, for the server settings see <a href='https://docs.webarch.net/wiki/Email'>our documentation</a>."
```

If you need to delete a status update or fix a typo edit the `status.html` file and commit your changes, the `index.html` file is rebuilt on every commit: 

```bash
cd ~/webarch-status
vim status.html
git commit -am "Status updated"
git push
```

## SSH

If you don't have a ssh key for the machine you want to update the site from then generate one:

```
ssh-keygen -t rsa -b 2048
```

And upload the `~/.ssh/id_rsa.pub` file to GitLab and then follow the install instructions above.

## HTTPS

The https://www.webarch.info/ site has a Gandi TLS certificate, the private key and csr were generated like this:

```bash
openssl req -nodes -newkey rsa:2048 -sha256 -keyout webarch-status.key.pem -out webarch-status.csr.pem
```
Then the certificate and the intermediate certificates from https://www.gandi.net/static/CAs/GandiStandardSSLCA2.pem and private key were uploaded to GitLab.

Note that the certificate field at GitLab takes the cert followed by the intermediate certificates. 

## DNS

This is the bind 9 zone file at https://gandi.net/

```bind
@ 10800 IN A 104.208.235.32
www 10800 IN A 104.208.235.32
```

We are using the Gandi DNS servers so that if the Webarchitects DNS servers are down the https://webarch.info/ site will still be up.

## TODO

1. Generate a RSS feed. 
2. Add spell checking for status updates.
